<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// *** AUTH ROUTES ***
// These routes are protected with de middlware auth.
// Visitor always need to authenticate.
Route::group(['middleware' => 'auth'], function () {

	Route::get('/', 'DashboardController@index'); //-- Overview	

	Route::get('/group', 'GroupController@index'); //-- Overview	
	Route::get('/group/create', 'GroupController@create'); //-- Create
	Route::post('/group/create', 'GroupController@store'); //-- Store

	Route::get('/slope', 'SlopeController@index'); //-- Select group	
	Route::get('/slope/overview/{id}', 'SlopeController@overview'); //-- Overview
	Route::get('/slope/edit/{id}', 'SlopeController@edit'); //-- Edit
});