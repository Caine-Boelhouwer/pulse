<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\SkiSetting;
use App\Models\Group;
use App\Models\Slope;
use App\Models\GroupUser;
use App\User;
use Auth;
use Validator;
use DateTime;
use DateInterval;
use DatePeriod;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myGroups = [];
        $currentUser = Auth::user()->id;
        $groups = GroupUser::where('users_id', '=', $currentUser)->get();

        foreach ($groups as $group) {
            $groupMembersArray = [];
            $groupLeader = '';
            $currentGroup = Group::where('id', '=', $group->group_id)->first();
            $groupMembers = GroupUser::where('group_id', '=', $currentGroup->id)->get();

            foreach ($groupMembers as $groupMember){
                $currentGroupMember = User::where('id', '=', $groupMember->users_id)->first();

                if ($currentGroup->leader_id == $currentGroupMember->id) {
                    $groupLeader = $currentGroupMember->name;
                } else {
                    $groupMembersArray += [$groupMember->users_id => $currentGroupMember->name];
                }
            }

            array_push($myGroups, [
                'leader' => $groupLeader,
                'name' => $currentGroup->name,
                'members' => $groupMembersArray
            ]);
        }

 
        // Return items to overview view
        return view('group.overview')
            ->with('myGroups', $myGroups);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $currentUser = Auth::user()->id;

        // Get al users and add default value
        $allUsers = ['default' => 'Select someone'] + User::where('id', '<>', $currentUser)->lists('name', 'id')->toArray();
        $day = ['default' => 'Day'];
        $month = ['default' => 'Month'];
        $skiAreas = [
            'default' => 'Select area',
            'Morzine' => 'Morzine',
            'Val thorens' => 'Val thorens',
            'Kirchberg' => 'Kirchberg',
            'Zell am see' => 'Zell am see',
            'Gerlos' => 'Gerlos'
        ];

        for ($i=1; $i < 32; $i++) { // Set all days in array
            $day += [$i => $i];
        }

        for ($i=1; $i < 13; $i++) { // Set all months in array
            $month += [$i => $i];
        }

        // Return items to overview view
        return view('group.add')
            ->with('day', $day)
            ->with('month', $month)
            ->with('allUsers', $allUsers)
            ->with('skiAreas', $skiAreas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currentUser = Auth::user()->id;

        // // Set rules to validate
        // $rules = [
        //     'name' => 'required',
        //     'area' => 'not_in:default',
        //     'startDateDay' => 'not_in:day',
        //     'startDateMonth' => 'not_in:month',
        //     'endDateDay' => 'not_in:day',
        //     'endDateMonth' => 'not_in:month',
        // ];

        // // Do validation
        // $validator = Validator::make($request->all(), $rules);

        // // Check if validator fails
        // if ($validator->fails()) {
        //     return redirect('group/create')
        //         ->withErrors($validator)
        //         ->withInput()
        //         ->with('notifyBox', 'active')
        //         ->with('notifyBoxContent', trans('general.notify_box_validation_fail'))
        //         ->with('notifyBoxType', 'alert-danger')
        //         ->with('notifyBoxIcon', 'warning');
        // }


        $startDate = date("Y").'-'.$request->startDateMonth.'-'.$request->startDateDay;
        $endDate = date("Y").'-'.$request->endDateMonth.'-'.$request->endDateDay;

        $begin = new DateTime($startDate);
        $end = new DateTime($endDate);

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        // Store data in database
        $skiSetting = SkiSetting::create([
            'area' => $request->area,
            'start_date' => $startDate,
            'end_date' => $endDate
        ]);

        // Store data in database
        $group = Group::create([
            'leader_id' => $currentUser,
            'ski_settings_id' => $skiSetting->id,
            'name' => $request->name
        ]);

        foreach ($request->groupMembers as $key => $value) {
            GroupUser::create([
                'group_id' => $group->id,
                'users_id' => $value
            ]);
        }

        foreach ($period as $date){
            Slope::create([
                'group_id' => $group->id,
                'isGoing' => false,
                'date' => $date->format("Y-m-d"),
                'votes' => 0
            ]);
        }

        return redirect('group')
            ->with('notifyBox', 'active')
            ->with('notifyBoxContent', trans('general.notify_box_add_success', ['item' => $request->name]))
            ->with('notifyBoxType', 'alert-success')
            ->with('notifyBoxIcon', 'done');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
