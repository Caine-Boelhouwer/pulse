<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\SkiSetting;
use App\Models\Group;
use App\Models\GroupUser;
use App\Models\Slope;
use App\User;
use Auth;
use Validator;

class SlopeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myGroups = [];
        $currentUser = Auth::user()->id;
        $groups = GroupUser::where('users_id', '=', $currentUser)->get();

        foreach ($groups as $group) {
            $currentGroup = Group::where('id', '=', $group->group_id)->first();

            array_push($myGroups, [
                'id' => $group->group_id,
                'name' => $currentGroup->name
            ]);
        }

 
        // Return items to overview view
        return view('slope.select')
            ->with('myGroups', $myGroups);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function overview($id)
    {
        $group = Group::where('id', '=', $id)->first();
        $slopes = Slope::where('group_id', '=', $id)->get();

        // Return items to overview view
        return view('slope.overview')
            ->with('slopes', $slopes)
            ->with('groupName', $group->name);
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slope = Slope::where('id', '=', $id)->first();

        // Return items to edit view
        return view('slope.edit')
            ->with('slope', $slope);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
