<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Notify box Language Lines
	|--------------------------------------------------------------------------
	|
	*/

	'notify_box_validation_fail' 	=> 'Niet alle velden zijn correct ingevuld.',
	'notify_box_add_success' 		=> '<strong>:item</strong> is met success toegevoegd.',
	'notify_box_edit_success' 		=> '<strong>:item</strong> is met success bijgewerkt.',
	'notify_box_delete_success' 	=> '<strong>:item</strong> is met success verwijderd.',
	'notify_box_dimension_success' 	=> 'De <strong>afmetingen</strong> zijn met success bewerkt.',
	'notify_box_edit_fail' 			=> 'Het item kon niet worden bijgewerkt omdat het opgegeven id niet voorkomt in de database.',
];