<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Notify box Language Lines
	|--------------------------------------------------------------------------
	|
	*/

	'notify_box_validation_fail' 	=> 'Not all the required fields are filled in.',
	'notify_box_add_success' 		=> '<strong>:item</strong> added successfully.',
	'notify_box_edit_success' 		=> '<strong>:item</strong> updated successfully.',
	'notify_box_delete_success' 	=> '<strong>:item</strong> deleted successfully.',
	'notify_box_edit_fail' 			=> 'You are using the wrong id...',
];