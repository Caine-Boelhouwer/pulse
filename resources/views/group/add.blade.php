@extends('layouts.master')

@section('title', 'Group')

@section('content')

    <div class="well bs-component">
        <fieldset>
            <div class="text-center">
            	<h3 class="no-border">Create group</h3>

				<div>
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#invite" aria-controls="invite" role="tab" data-toggle="tab">Invite</a></li>
						<li role="presentation"><a href="#area" aria-controls="area" role="tab" data-toggle="tab">Area</a></li>
						<li role="presentation"><a href="#date" aria-controls="date" role="tab" data-toggle="tab">Date</a></li>
					</ul>

                	{!! Form::open(['url' => '/group/create', 'class' => 'form-horizontal']) !!}
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="invite">
							<h4>Making the group</h4>

	                        <div class="form-group @if ($errors->has('name')) has-error @endif">
	                            <div class="col-md-10">
	                                {!! Form::text('inputGroupName', null, ['name' => 'name', 'class' => 'form-control', 'placeholder' => 'Group name']) !!}
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <div class="col-md-10">
									{!! Form::select('allUsers', $allUsers, 'default', ['class' => 'form-control', 'id' => 'add-selector']) !!}
	                            </div>
	                        </div>

			            	<div id="add-list" class="group-box">
			            		<div class="group-leader">{{ Auth::user()->name }}</div>			            		
			            	</div>
			            	{!! Form::hidden('', Auth::user()->id, ['name' => 'groupMembers[]']) !!}
						</div>

						<div role="tabpanel" class="tab-pane fade" id="area">
							<h4>Select ski area</h4>
	                        <div class="form-group @if ($errors->has('area')) has-error @endif">
	                            <div class="col-md-10">
	                                {!! Form::select('skiAreas', $skiAreas, 'default', ['name' => 'area', 'class' => 'form-control']) !!}
	                            </div>
	                        </div>
						</div>

						<div role="tabpanel" class="tab-pane fade" id="date">
							<h4>Date</h4>
							<div class="row">
								<div class="col-xs-4">
									Start date
								</div>
								<div class="col-xs-4">
								</div>
								<div class="col-xs-4">
									End date
								</div>

								<div class="col-xs-2 @if ($errors->has('startDateDay')) has-error @endif">
									{!! Form::select('day', $day, 'default', ['name' => 'startDateDay','class' => 'form-control text-center']) !!}
								</div>
								<div class="col-xs-2 @if ($errors->has('startDateMonth')) has-error @endif">
									{!! Form::select('month', $month, 'default', ['name' => 'startDateMonth', 'class' => 'form-control text-center']) !!}
								</div>
								<div class="col-xs-4">
									<div class="form-group"><div class="form-control text-center">-</div></div>
								</div>
								<div class="col-xs-2 @if ($errors->has('endDateDay')) has-error @endif">
									{!! Form::select('day', $day, 'default', ['name' => 'endDateDay', 'class' => 'form-control text-center']) !!}
								</div>
								<div class="col-xs-2 @if ($errors->has('endDateMonth')) has-error @endif">
									{!! Form::select('month', $month, 'default', ['name' => 'endDateMonth', 'class' => 'form-control text-center']) !!}
								</div>
							</div>

							{!! Form::button('Create', ['class' => 'btn btn-raised btn-primary', 'type' => 'submit']) !!}
						</div>
					</div>
	                {!! Form::close() !!}
				</div>

            </div>
        </fieldset>
    </div>  
@endsection

