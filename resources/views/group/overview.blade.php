@extends('layouts.master')

@section('title', 'Group')

@section('content')

    <div class="well bs-component">
		<a href="{{ url('/group/create') }}" class="btn btn-success btn-fab pull-right add-btn"><i class="material-icons">add</i></a>

        <fieldset>
            <div class="text-center">
            	<h3>My groups</h3>

                @foreach ($myGroups as $myGroup)
                    <div class="group-box">
                        <h4 class="group-title">{{$myGroup['name']}}</h4>
                        <div class="group-leader">{{$myGroup['leader']}}</div>
                        @foreach ($myGroup['members'] as $key => $value)
                            <div class="group-member">{{$value}}</div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </fieldset>
    </div>   

@endsection

