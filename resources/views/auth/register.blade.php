@extends('layouts.auth')

@section('title', 'Create account')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="well bs-component">

				{!! Form::open(['url' => '/auth/register', 'class' => 'form-horizontal']) !!}
                    <fieldset id="auth">
                        <legend>Pulse</legend>
						
						<div class="page-action">Create account</div>

                        <div class="form-group @if ($errors->has('email')) has-error @endif">
                            {!! Form::label('inputName', 'Name', ['class' => 'col-md-2 control-label']) !!}

                            <div class="col-md-10">
                                {!! Form::text('inputName', null, ['name' => 'name', 'class' => 'form-control', 'placeholder' => '']) !!}
                            </div>
                        </div>		

                        <div class="form-group @if ($errors->has('email')) has-error @endif">
                            {!! Form::label('inputEmail', 'E-mail', ['class' => 'col-md-2 control-label']) !!}

                            <div class="col-md-10">
                                {!! Form::text('inputEmail', null, ['name' => 'email', 'class' => 'form-control', 'placeholder' => '']) !!}
                            </div>
                        </div>

                        <div class="form-group @if ($errors->has('password')) has-error @endif">
                            {!! Form::label('inputPassword', 'Password', ['class' => 'col-md-2 control-label']) !!}

                            <div class="col-md-10">
                                {!! Form::password('password', ['name' => 'password', 'class' => 'form-control', 'placeholder' => '']) !!}
                            </div>
                        </div>

                        <div class="form-group @if ($errors->has('password')) has-error @endif">
                            {!! Form::label('inputConfirmPassword', 'Confirm Password', ['class' => 'col-md-2 control-label']) !!}

                            <div class="col-md-10">
                                {!! Form::password('password', ['name' => 'password_confirmation', 'class' => 'form-control', 'placeholder' => '']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                {!! Form::button('Create', ['class' => 'btn btn-raised btn-primary', 'type' => 'submit']) !!}
                                <a class="create-acc-btn" href="/auth/login">Back to login</a>
                            </div>
                        </div>
                    </fieldset>
                {!! Form::close() !!}

            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
@endsection
