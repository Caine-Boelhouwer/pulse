@extends('layouts.auth')

@section('title', 'Inloggen')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="well bs-component">

                {!! Form::open(['url' => '/auth/login', 'class' => 'form-horizontal']) !!}
                    <fieldset id="auth">
                        <legend>Pulse</legend>
                        
                        <div class="page-action">Login</div>

                        <div class="form-group @if ($errors->has('email')) has-error @endif">
                            {!! Form::label('inputEmail', 'E-mail', ['class' => 'col-md-2 control-label']) !!}

                            <div class="col-md-10">
                                {!! Form::text('inputEmail', null, ['name' => 'email', 'class' => 'form-control', 'placeholder' => '']) !!}
                            </div>
                        </div>

                        <div class="form-group @if ($errors->has('password')) has-error @endif">
                            {!! Form::label('inputPassword', 'Wachtwoord', ['class' => 'col-md-2 control-label']) !!}

                            <div class="col-md-10">
                                {!! Form::password('password', ['name' => 'password', 'class' => 'form-control', 'placeholder' => '']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('remember', null, true); !!} Onthoud mij
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                {!! Form::button('Login', ['class' => 'btn btn-raised btn-primary', 'type' => 'submit']) !!}
                                <a class="create-acc-btn" href="/auth/register">Or create account</a>
                            </div>
                        </div>
                    </fieldset>
                {!! Form::close() !!}
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

@endsection
