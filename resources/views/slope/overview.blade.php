@extends('layouts.master')

@section('title', 'Slope')

@section('content')

    <div class="well bs-component">
        <fieldset>
            <div class="text-center">
            	<h3>{{$groupName}}</h3>

                <div class="slope-box">
					<div class="row">
						<div class="col-xs-4">
							Date
						</div>
						<div class="col-xs-4">
							Votes
						</div>
						<div class="col-xs-4">
							Edit
						</div>
					</div>
					@foreach ($slopes as $slope)
						<div class="row slope">
							<div class="col-xs-4">
								{{date('d-m', strtotime($slope->date))}}
							</div>
							<div class="col-xs-4">
								@if ($slope->isGoing)
									{{$slope->votes}}
								@else
									No ski day
								@endif
							</div>
							<div class="col-xs-4">
								<a href="{{url('slope/edit')}}/{{$slope->id}}"><i class="material-icons">border_color</i></a>
							</div>
						</div>
					@endforeach
                </div>
            </div>
        </fieldset>
    </div>   

@endsection

