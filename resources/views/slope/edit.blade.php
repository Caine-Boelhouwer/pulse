@extends('layouts.master')

@section('title', 'Slope')

@section('content')

    <div class="well bs-component">
        <fieldset>
            <div class="text-center">
            	<h3>Slope / {{date('d-m-Y', strtotime($slope->date))}}</h3>

            	<img id="choose_slope" src="{{url('img/piste-xsmall.png')}}">
                </div>
            </div>
        </fieldset>
    </div>   

@endsection

