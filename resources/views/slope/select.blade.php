@extends('layouts.master')

@section('title', 'Slope')

@section('content')

    <div class="well bs-component">
        <fieldset>
            <div class="text-center">
            	<h3>My slopes</h3>

                @foreach ($myGroups as $myGroup)
                    <div class="group-box slope">
                        <a href="{{url('/slope/overview')}}/{{$myGroup['id']}}" class="group-title">{{$myGroup['name']}}</a href="{{$myGroup['id']}}">
                    </div>
                @endforeach
            </div>
        </fieldset>
    </div>   

@endsection

