<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">    
    <meta name="csrf_token" content="{!! csrf_token() !!}">

	<title>Pulse | @yield('title')</title>

    <!-- Material Design fonts -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Custom Fonts -->
    <link href="{{ asset('/css/vendor/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('/css/vendor/bootstrap.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Material Design CSS -->
    <link href="{{ asset('/css/vendor/bootstrap-material-design.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/vendor/ripples.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" type="text/css">

</head>
<body>
	<div id="wrapper">
        
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <div class="navbar-brand"><i class="material-icons">menu</i>Pulse</div>
            </div>
            <!-- /.navbar-header -->
        </nav>
    	
        <div id="side-menu">
            <div class="header text-center">
                <i class="material-icons">account_circle</i>
                <div class="name">{{ Auth::user()->name }}</div>
                <div class="role"><a href="{{ url('/auth/logout') }}">logout</a></div>
            </div>  
            <ul>
                <li><a href="{{ url('/') }}">Dashboard</a></li>
                <li><a href="{{ url('/group') }}">Groups</a></li>
                <li><a href="{{ url('/slope') }}">Slopes</a></li>
            </ul>
        </div>

        @if (session('notifyBox'))
            <div id="notify-box" class="{{ session('notifyBox') }}">
                <div class="alert {{ session('notifyBoxType') }}">
                    <i class="material-icons">{!! session('notifyBoxIcon') !!}</i> {!! session('notifyBoxContent') !!}      
                </div>
            </div>
        @endif

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <!-- Content of the requested view -->
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{ asset('/js/vendor/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('/js/vendor/bootstrap.min.js') }}"></script>

    <!-- Modal dialog confirm Plugin JavaScript -->    
    <script src="{{ asset('/js/vendor/jquery.confirm.min.js') }}"></script>

    <!-- Meterial JavaScript -->
    <script src="{{ asset('/js/vendor/ripples.min.js') }}"></script>
    <script src="{{ asset('/js/vendor/material.min.js') }}"></script>

    <!-- Custom Theme JavaScript -->   
    <script src="{{ asset('/js/script.js') }}"></script>
</body>
</html>
