$(document).ready(function() {
    $.material.init();

    $(".defaultConfirm").confirm({
        confirmButton: "<i class='material-icons'>delete</i> Ja",
        cancelButton: "Nee",
        confirmButtonClass: "btn-danger btn-raised",
        post: false
    });

    $(".form-group").find(":input").on("click", function(){
        if ($(this).closest(".form-group").hasClass("has-error")){
            $(this).closest(".form-group").removeClass("has-error");
        } else {
            $(this).closest(".form-group").find(".has-error").each(function(i, obj) {
                $(obj).removeClass("has-error");
            });
        }

        $(this).closest(".form-group").find(".help-block").remove();
    });

    slideMenu();
    createClickList();

    // Shows notify box with data
    notify();


});

function createClickList(){

    $("#add-selector").change(function() {
        $( "#add-selector option:selected" ).each(function() {
            var optionValue = $(this).val();
            var isSelected = false;

            $('#add-list div').each(function () {
                if (this.id == optionValue){
                    isSelected = true;
                }
            });

            if ($(this).val() != 'default' && isSelected == false){
                $("<div class='group-member'>"+$(this).text()+"</div>").appendTo("#add-list");
                $("#add-list").after("<input type='hidden' value='"+$(this).val()+"' name='groupMembers[]'>");
                $(this).remove();
            }
        });
    }).trigger( "change" );
}

function slideMenu(){
    $(".navbar-brand").on('click', function(event) {
        event.preventDefault();

        var html = "<div id='shade'></div>";

        $(html).hide().appendTo("body").fadeIn(350);
        $("#side-menu").animate({"left": '+=245px'});

        $("#shade").on('click', function(event) {
            event.preventDefault();

            $(this).fadeOut(350, function(event){
                $(this).remove();
            });

            $("#side-menu").animate({"left": '-245px'});
        });
    });
}

function notify(){
    if ($("#notify-box").hasClass("active")){

        $("#notify-box")
            .hide()
            .fadeIn(500, function(event){})
            .delay(4500)
            .fadeOut(500, function(event) {
                $(this).remove();
            });
    }

}